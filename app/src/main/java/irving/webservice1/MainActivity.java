package irving.webservice1;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.support.v4.app.Fragment;

import irving.webservice1.Fragment.Formulario;
import irving.webservice1.Fragment.Inicio;
import irving.webservice1.Fragment.Solicitud;
import irving.webservice1.Interface.Interfaces;

public class MainActivity extends AppCompatActivity implements Interfaces
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment inicio = new Inicio();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.ani, R.anim.ani).replace(R.id.main, inicio).commit();
        this.setTitle("Inicio");
    }

    public void Formulario(View v)
    {
        Formulario fo = new Formulario();
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main,  fo, "fragment_meters");
        ft.addToBackStack(null);
        ft.commit();
        this.setTitle("Formulario");
    }

    public void Solicitud(View v)
    {
        Solicitud so = new Solicitud();
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main,  so, "fragment_meters");
        ft.addToBackStack(null);
        ft.commit();
        this.setTitle("Solicitud");
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}