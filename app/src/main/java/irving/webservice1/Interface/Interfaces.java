package irving.webservice1.Interface;

import irving.webservice1.Fragment.Formulario;
import irving.webservice1.Fragment.Inicio;
import irving.webservice1.Fragment.Solicitud;

public interface Interfaces extends Formulario.OnFragmentInteractionListener,
        Inicio.OnFragmentInteractionListener,
        Solicitud.OnFragmentInteractionListener
{ }
