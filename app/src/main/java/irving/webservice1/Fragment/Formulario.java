package irving.webservice1.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import irving.webservice1.R;


public class Formulario extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private EditText campo_rfc;
    private EditText campo_nombre;
    private EditText campo_telefono;
    private Button listo;

    public Formulario() { }

    public static Formulario newInstance(String param1, String param2) {
        Formulario fragment = new Formulario();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        getActivity().setTitle("Formulario");
        View v = inflater.inflate(R.layout.fragment_formulario, container, false);

        campo_rfc = v.findViewById(R.id.rfc);
        campo_nombre = v.findViewById(R.id.etNombre);
        campo_telefono = v.findViewById(R.id.etTelefono);
        listo = v.findViewById(R.id.btnListoFo);

        listo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String rfc = campo_rfc.getText().toString();
                String nombre = campo_nombre.getText().toString();
                String validaTelefono = campo_telefono.getText().toString();
                int telefono = 0;
                try {
                    telefono  = Integer.parseInt(campo_telefono.getText().toString());
                }catch (Exception e){ }


                if(!rfc.isEmpty() && !nombre.isEmpty() && !validaTelefono.isEmpty())
                {
                    Snackbar.make(v, "Listo xd", Snackbar.LENGTH_LONG).show();
                        //// CERRAR FRAGMENT, REGRESAR AL FRAGMENT ANTERIOR ////
                    getActivity().getSupportFragmentManager().popBackStack();
                }
                else
                {
                    Toast.makeText(getContext(), "Debes llenar los campos", Toast.LENGTH_LONG).show();
                }
            }
        });

        return v;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}