package irving.webservice1.Fragment;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import irving.webservice1.R;

public class Solicitud extends Fragment {

    private EditText campo_nombre, campo_telefono, campo_rfc;
    private Button solicitar;

    public Solicitud() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_solicitud, container, false);

        campo_rfc = v.findViewById(R.id.rfcSoli);
        solicitar = v.findViewById(R.id.buttonSoli);
        campo_nombre = v.findViewById(R.id.etNombreMostrar);
        campo_telefono = v.findViewById(R.id.etTelefonoMostrar);

        campo_nombre.setEnabled(false);
        campo_telefono.setEnabled(false);

        solicitar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String rfc = campo_rfc.getText().toString();
                if(!rfc.isEmpty())
                {

                }
                else
                {
                    Toast.makeText(getContext(), "Debes ingresar el RFC", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}